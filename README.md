# 1 Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    .white {
        BackgroundColor white
    }
    boxless {
        FontColor darkgreen
    }
}
</style>
* Programación Declarativa
**_ surge
*** En consecuencia a problemas que lleva\n consigo la programación clásica
**** Programación Inperativa <<white>>
***** Caracteristicas
****** Basado en la arquitectura de Von Newmann <<white>>
****** Requiere gestionar la memoria del ordenador <<white>>
****** Secuencia instrucciones concretas y muy sencillas <<white>>
***** Lenguaje de bajo nivel 
****** Ensambrador <<white>>
***** Lenguaje de alto nivel
****** C <<white>>
****** Pascal <<white>>
**_ ventajas
*** Programas más cortos
*** Más fáciles de ralizar
*** Más fáciles de depurar
** Programador <<white>>
*** Creativo
**** Se da prioridad a la idea del algoritmo para resolver un problema <<white>>
*** Burocratico
**** Eficiencia de los tiempo <<white>>
**** Gestión detallada de la memoria <<white>>
** Tipos <<white>>
*** Programación\n Funcional
****_ se basa
***** Tomar el modelo en el que los matemáticos definen funciones <<white>>
****_ consiste
***** Funciones <<white>>
****** Los programas se entienden como funciones Matemáticas
******_ tiene
******* Datos de entrada
******* Datos de salida
****_ funciona\n mendiante 
***** Aplicar funciones a datos complejos <<white>>
***** Mediante reglas de simplificacion <<white>>
****_ ventajas
***** Funciones de Orden Superior <<white>>
******_ consiste en 
******* La entrada de un programa puede ser otro programa
***** Evaluación Perezosa <<white>>
******_ consiste en
******* La evaluación que retrasa el cálculo de una expresión hasta que su valor sea necesario
****_ ejemplos
***** LISP
***** Haskell
*** Programación\n Lógica
****_ se basa
***** En el modelo de la demostración de la lógica y la demostración automática <<white>>
****_ consiste
***** Sistema lógico <<white>>
****** Axiomas
****** Reglas de inferencia
****** Predicado
******* Relaciones entre objetos que estamos definiendo
******* Las relaciones no establece un orden entre argumentos de entrada y de salida
****_ ejemplos
***** Prolog


@endmindmap
```
# 2 Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    .white {
        BackgroundColor white
    }
    boxless {
        FontColor darkgreen
    }
}
</style>
* Programación Funcional
**_ es un 
*** Lenguaje de Programación <<white>>
****_ es un
***** Paradigma
****** Basado en el modelo de Von Newman <<white>>
******* Los programas debían almacenarse en la misma máquina antes de ser ejecutados
******* La ejecución consistiría en una serie de instrucciones que serían ejecutados secuencialmente en secuencias
******* La memoria se puede acceder mediante una serie de variables
******* Las variables pueden ser modificadas mediante lo que se llama una instrucción de asignación
****_ se clasifican
***** Imperativos 
****** Programación Orientada a Objetos <<white>>
******* El código son objetos que están interactuando entre sí
******* Los objetos se componen de instruciones que se ejecuntan secuencialmente
******_ caracteristicas
******* Evaluación estricta <<white>>
******** Evalua las expresiones desde dentro hacia fuera
******* Evaluación no estricta <<white>>
******** Evalua desde fuera hacia dentro
***** Declarativos
****** Programación Lógica <<white>>
******* Un conjunto de sentencias que definen lo que es verdad
****** Programación Funcional <<white>>
****_ se usa\n para crear
***** Programas
******_ caracteristicas
******* Es una colección de datos <<white>>
******* Una serie de instrucciones que operan sobre dichos datos <<white>>
******* La ejecución de las instrucciones en ese orden determinado a realizar un determinado cómputo <<white>>
**_ se basa en
*** Función <<white>>
**** Concepción puramente matemática
****_ basado
***** Cálculo Lambda <<white>>
******_ propuesto por
******* Church en 1930
******** Inicialmente pensado como un sistema para estudiar el concepto de función y sus aplicaciones recursivas
******_ consiste
******* En realizar evaluaciónes de expresiones usando funciones como mecanismos de cómputo
******_ características
******* Una función recibe un parametro de entrada
******* Una función devuelve un parametro de salida
******* El parametro de entrada y salida pude ser una función
**_ ventajas
*** Transparencia Referencial <<white>>
****_ significa
***** Los resultados de las funciones son idependientes del orden en el que se realizan los cálculos
****_ describe 
***** Una función recibe los mismos parámetros de entrada
***** Una función siempre va a devolver el mismo valor
**_ Caracteristicas
*** Función de Orden Superior <<white>>
****_ consiste
***** El parametro de entrada de una función puede ser otra función
**** Evaluación Perezosa <<white>>
*****_ consiste 
****** Calcula una expresión (parcial) solamente si realmente\n se necesita el valor para calcular el resultado
***** Memorización <<white>>
******_ consiste
******* Recordar todo lo que ya se ha evaluado para no volverlo a evaluar
 
@endmindmap
```